-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  Dim 07 avr. 2019 à 22:39
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ecole`
--

-- --------------------------------------------------------

--
-- Structure de la table `chambre`
--

CREATE TABLE `chambre` (
  `no` int(11) NOT NULL COMMENT 'Numéro identifiant de la chambre de l''élève',
  `prix` float NOT NULL COMMENT 'Prix de location de la chambre'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des chambres éventuellement associées aux élèves.';

--
-- Déchargement des données de la table `chambre`
--

INSERT INTO `chambre` (`no`, `prix`) VALUES
(1, 200.12),
(2, 400.55),
(3, 350.25),
(4, 400.55),
(5, 250.45),
(6, 150.75),
(7, 200.25),
(8, 45),
(9, 344),
(10, 150),
(11, 78),
(12, 333),
(13, 78),
(14, 150),
(15, 78);

-- --------------------------------------------------------

--
-- Structure de la table `eleve`
--

CREATE TABLE `eleve` (
  `num` varchar(100) NOT NULL COMMENT 'Numéro identifiant l''elève',
  `no` int(11) DEFAULT NULL COMMENT 'Numéro de la chambre de l''élève',
  `nom` varchar(50) DEFAULT NULL COMMENT 'Nom de l''élève',
  `age` tinyint(4) DEFAULT NULL COMMENT 'Age de l''élève',
  `adresse` varchar(200) DEFAULT NULL COMMENT 'Adresse de l''élève'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table contenant l''ensemble des élèves';

--
-- Déchargement des données de la table `eleve`
--

INSERT INTO `eleve` (`num`, `no`, `nom`, `age`, `adresse`) VALUES
('AGUE001', 1, 'AGUE MAX', 40, '18 Rue Labat 75018 Paris'),
('KAMTO005', 1, 'KAMTO Dione', 50, '54 Rue des Ebisoires 78300 Poissy'),
('LAURENCY004', NULL, 'LAURENCY Patrick', 52, '79 Rue des Poules 75015 Paris'),
('TABIS003', NULL, 'Ghislaine TABIS', 30, '12 Rue du louvre 75013 Paris'),
('TAHAE002', NULL, 'TAHA RIDENE', 30, '12 Rue des Chantiers 78000 Versailles');

-- --------------------------------------------------------

--
-- Structure de la table `inscrit`
--

CREATE TABLE `inscrit` (
  `code` varchar(100) NOT NULL COMMENT 'Id Code de l''uv',
  `num` varchar(100) NOT NULL COMMENT 'Numéro identifiant de l''élève',
  `note` float DEFAULT NULL COMMENT 'Note accordée à un élève sur une matière ou UV'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table recapitulant les élèves inscrit aux différents UV';

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

CREATE TABLE `livre` (
  `cote` varchar(100) NOT NULL COMMENT 'Numéro identifiant du livre',
  `num` varchar(100) DEFAULT NULL COMMENT 'Numéro identifiant de l''élève qui a emprunté le livre',
  `titre` varchar(100) NOT NULL COMMENT 'Titre du livre',
  `datepret` datetime DEFAULT NULL COMMENT 'Date et heure du pret du livre par l''élève'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table contenant les livres éventuellement associés aux élèves.';

--
-- Déchargement des données de la table `livre`
--

INSERT INTO `livre` (`cote`, `num`, `titre`, `datepret`) VALUES
('ISBN10000', 'LAURENCY004', 'Un vase d\'honneur', '2019-03-22 13:00:00'),
('ISBN10001', 'TABIS003', 'Seul au monde', '2019-04-10 08:18:00'),
('ISBN10002', 'TAHAE002', 'Meutre à la maison blanche', '2019-04-01 09:18:00'),
('ISBN10003', 'AGUE001', 'Double Impact', '2019-04-24 09:22:00');

-- --------------------------------------------------------

--
-- Structure de la table `uv`
--

CREATE TABLE `uv` (
  `code` varchar(100) NOT NULL COMMENT 'Id Code de l''uv',
  `nbh` tinyint(3) NOT NULL COMMENT 'Nombre d''heure de cour',
  `coord` varchar(255) DEFAULT NULL COMMENT 'COORD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des unités de valeurs ou d''enseignement';

--
-- Déchargement des données de la table `uv`
--

INSERT INTO `uv` (`code`, `nbh`, `coord`) VALUES
('JAVA_Grp1', 30, 'Mr RIDENE'),
('maths_info_Grp1', 26, 'Mme ASSELAH'),
('Web_Service_Grp1', 10, 'Mr PLASSE');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `chambre`
--
ALTER TABLE `chambre`
  ADD PRIMARY KEY (`no`);

--
-- Index pour la table `eleve`
--
ALTER TABLE `eleve`
  ADD PRIMARY KEY (`num`),
  ADD KEY `Eleve_index_Chambre` (`no`);

--
-- Index pour la table `inscrit`
--
ALTER TABLE `inscrit`
  ADD PRIMARY KEY (`code`,`num`),
  ADD KEY `inscrit_index_Eleve` (`num`),
  ADD KEY `inscrit_index_Uv` (`code`);

--
-- Index pour la table `livre`
--
ALTER TABLE `livre`
  ADD PRIMARY KEY (`cote`),
  ADD KEY `Livre_index_Eleve` (`num`);

--
-- Index pour la table `uv`
--
ALTER TABLE `uv`
  ADD PRIMARY KEY (`code`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `eleve`
--
ALTER TABLE `eleve`
  ADD CONSTRAINT `Eleve_fk1` FOREIGN KEY (`no`) REFERENCES `chambre` (`no`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `inscrit`
--
ALTER TABLE `inscrit`
  ADD CONSTRAINT `inscrit_fk1` FOREIGN KEY (`num`) REFERENCES `eleve` (`num`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inscrit_fk2` FOREIGN KEY (`code`) REFERENCES `uv` (`code`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `livre`
--
ALTER TABLE `livre`
  ADD CONSTRAINT `livre_fk1` FOREIGN KEY (`num`) REFERENCES `eleve` (`num`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
