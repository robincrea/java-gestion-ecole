package com.crea.dev1.jee.ecole.model.beans;

public class Inscrit {
	private String code; // Id code de l'uv
	private String num; // Numéro identifiant élève
	private float note; // Note accpdée à un élève sur matière ou UV
	
	/**
	 * Contructor
	 * @param code
	 * @param num
	 * @param note
	 */
	public Inscrit(String code, String num, float note) {
		this.code = code;
		this.num = num;
		this.note = note;
	}
	// Init
	public Inscrit() {
		this("","", 0);
	}

	/**
	 * Getters And Setters
	 */
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public float getNote() {
		return note;
	}
	public void setNote(float note) {
		this.note = note;
	}
	

	
	
}
