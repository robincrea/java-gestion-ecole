package com.crea.dev1.jee.ecole.model.beans;

public class UV {
	private String code; // Id code de l'uv
	private int nbh; // Nombre d'heur de cours
	private String coord;
	
	/**
	 * CONSTRUCTORS
	 * @param code
	 * @param nbh
	 * @param coord
	 */
	public UV(String code, int nbh, String coord) {
		this.code = code;
		this.nbh = nbh;
		this.coord = coord;
	}
	
	// INIT
	public UV() {
		this("", 0,"");
	}

	
	/*
	 * Getters and setters
	 */
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getNbh() {
		return nbh;
	}

	public void setNbh(int nbh) {
		this.nbh = nbh;
	}

	public String getCoord() {
		return coord;
	}

	public void setCoord(String coord) {
		this.coord = coord;
	}

	
	
	
}
