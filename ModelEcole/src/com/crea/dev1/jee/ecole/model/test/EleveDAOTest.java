package com.crea.dev1.jee.ecole.model.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.crea.dev1.jee.ecole.model.beans.Eleve;
import com.crea.dev1.jee.ecole.model.dao.EleveDAO;


class EleveDAOTest {
	
	Eleve ref1 = new Eleve("ROB001", 0,"Robin Ferrari", 21, "5 Rue du véldrome, 1205 Genève");
	Eleve ref2 = new Eleve("DAV0001", 0,"Taha Ridene", 21, "5 Rue du véldrome, 1205 Genève");	
 
	@BeforeEach 
    public void testAdd() {
		assertTrue(EleveDAO.add(this.ref1));
		assertTrue(EleveDAO.add(this.ref2));
		assertFalse(EleveDAO.add(this.ref2));

    }
	@AfterEach
    public void testDelete() {
		assertTrue(EleveDAO.delete(ref1.getNum()));
		assertTrue(EleveDAO.delete(ref2.getNum()));
		assertFalse(EleveDAO.delete(ref2.getNum()));
    }

	
	@Test
	void testDisplayAll() {
		assertTrue(EleveDAO.displayAll());
	}

	@Test
	void testGetAll() {
		assertNotNull( EleveDAO.getAll());
		assertNotNull( EleveDAO.getAll().get(0).getNom());
	}

	@Test
	void testGetByNum() {
		assertNotNull(EleveDAO.getByNum(ref1.getNum()));
		assertEquals(ref1.getNum(), EleveDAO.getByNum(ref1.getNum()).getNum());
	}
	@Test
	void testMax() {
		assertNotNull(EleveDAO.getByNum("AGUE001"));
	}
	

	@Test
	void testGetByNom() {
		assertNotNull(EleveDAO.getByNom(ref1.getNom()));
		assertEquals(ref1.getNom(), EleveDAO.getByNom(ref1.getNom()).get(0).getNom());
	}

	@Test
	void testGetByAge() {
		ArrayList<Eleve> byAge = EleveDAO.getByAge(ref1.getAge());
		assertNotNull(byAge);
		Iterator<Eleve> elevesIterator = byAge.iterator();
		while(elevesIterator.hasNext()) {
			assertEquals(ref1.getAge(), elevesIterator.next().getAge() );
		}

	}
	
	@Test
	void testGetByNo() {
		ArrayList<Eleve> byNo = EleveDAO.getByNo(ref1.getNo());
		assertNotNull(byNo);
		Iterator<Eleve> elevesIterator = byNo.iterator();
		while(elevesIterator.hasNext()) {
			assertEquals(ref1.getNo(), elevesIterator.next().getNo() );
		}

	}

	@Test
	void testUpdateAdresseEleveByNum() {
		String adresse = "Rue du test 22";
		EleveDAO.updateAdresse(ref2.getNum(), adresse);
		assertEquals(adresse, EleveDAO.getByNum(ref2.getNum()).getAdresse());
	}

	@Test
	void testUpdNoChambreEleveByNum() {
		int chambre = 2;
		EleveDAO.updateChambre(ref2.getNum(), chambre);
		assertEquals(chambre, EleveDAO.getByNum(ref2.getNum()).getNo());
	}
	



}
