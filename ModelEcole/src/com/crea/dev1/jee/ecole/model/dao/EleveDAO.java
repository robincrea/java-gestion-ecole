package com.crea.dev1.jee.ecole.model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.crea.dev1.jee.ecole.model.beans.Eleve;
import com.crea.dev1.jee.ecole.model.common.DBUtils;

public class EleveDAO{
	
	/**
	 * Ajout d'un élève
	 * @param newEleve
	 * @throws SQLException
	 */
	public static boolean add(Eleve newEleve){
		// INSERT INTO `eleve` (`num`, `no`, `nom`, `age`, `adresse`) VALUES ('vic0001', '0', 'Victor', '29', 'rue du test');;
		String no;
		if(newEleve.getNo() == 0) {
			no = "NULL";
		}else {
			no = Integer.toString(newEleve.getNo());
		}
		String eleveValues ="'"+ newEleve.getNum() +"',"+ no +",'"+newEleve.getNom() +"','"+newEleve.getAge() +"','"+newEleve.getAdresse()+"'";
		
		// On prépare notre requete SQL
		String req = "INSERT INTO `eleve` (`num`, `no`, `nom`, `age`, `adresse`) VALUES ("+eleveValues+")";
			
		try {
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			DBUtils.getStm().executeUpdate(req) ;
			System.out.println("L'élève num: " + newEleve.getNum()  +" a bien été ajout");
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("L'élève num: " + newEleve.getNum()  +" n'a pas pu être ajouté");
			// e.printStackTrace();
			return false;
		}
	
	}
	
	
	/**
	 * Affiche tous les élèves
	 * @throws SQLException
	 */
	@SuppressWarnings("finally")
	public static boolean displayAll() {		
			
		boolean retour = false;
			ArrayList<Eleve> eleves = EleveDAO.getAll();
			
			Iterator<Eleve> elevesIterator = eleves.iterator();
			try {
				while(elevesIterator.hasNext()) {
					System.out.println("L'élève est "+elevesIterator.next().getNom() );
				}				
				retour =  true;

			}
			catch(Exception e) {
				retour = false;
			}
			finally {
				return retour;
			}				
	}
	
	/**
	 * créer une instance initialisé de Class Eleve avec les valeur retourné d'une requette SQL
	 * @param res résultat d'une requete SELECT sur la table élève
	 * @return Eleve
	 * @throws SQLException
	 */
	private static Eleve getFromRes(ResultSet res) throws SQLException {
		Eleve elevTemp = new Eleve();

		elevTemp.setNum(res.getString(1));
		elevTemp.setNo(res.getInt(2));
		elevTemp.setNom(res.getString(3));
		elevTemp.setAge(res.getInt(4));
		elevTemp.setAdresse(res.getString(5));
		
		return elevTemp;
	}
	
	/**
	 * retourne tous les élèves dans un arraylist
	 * @return ArrayList<Eleve> : Liste d'élève
	 * @throws SQLException
	 */
	public static ArrayList<Eleve> getAll(){
		// SELECT * FROM eleve
		// Connection con  = DBAction.getCon();

	    ArrayList<Eleve> listEleves = new ArrayList<Eleve>();

		// On prépare notre requete SQL
		String req = "SELECT num, no, nom, age, adresse FROM eleve";
	
		// DEMMARER LA CONNECTION
		DBUtils.DBConnexion();
		
		// Execution de la requete et init
		try {
			DBUtils.setRes(DBUtils.getStm().executeQuery(req));
			while(DBUtils.getRes().next()) {
				Eleve elevTemp = EleveDAO.getFromRes(DBUtils.getRes());
				elevTemp.affiche();
				listEleves.add(elevTemp);
			}
			// Stopper la connection
			DBUtils.DBClose();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return listEleves;
								
	}

	
	/**
	 * Récupère un arraylist contenant les élève d'un column donnée correspondant à une valeur donné
	 * @param column
	 * @param value
	 * @return
	 */
	private static ArrayList<Eleve> getBy(String column, String value){
		
		 ArrayList<Eleve> listEleves = new ArrayList<Eleve>();

		// On prépare notre requete SQL
		String req = "SELECT num, no, nom, age, adresse FROM eleve WHERE eleve."+column+"='"+value+"'";
		
		try {
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			DBUtils.setRes(DBUtils.getStm().executeQuery(req));
			
			while(DBUtils.getRes().next()) {
				Eleve elevTemp = EleveDAO.getFromRes(DBUtils.getRes());
				listEleves.add(elevTemp);
			}
			
			// Stopper la connection
			DBUtils.DBClose();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listEleves;
	}
	

	/**
	 * Séléction d'un élève avec un ID
	 * @param idEleve
	 * @return Eleve Retourne l'élève avec le num donné
	 */
	public static Eleve getByNum(String numEleve){
		return EleveDAO.getBy("num", numEleve.toString()).get(0);
	}
	
	/**
	 * Séléction d'élève avec un Nom
	 * @param idEleve
	 * @return ArrayList<Eleve> Retourne un array list d'élève avec un num donné
	 */
	public static ArrayList<Eleve> getByNom(String nom){
		return EleveDAO.getBy("nom", nom);
	}
	
	/**
	 * Retourn un arraylist des élève d'un age donné
	 * @param age
	 * @return ArrayList<Eleve>
	 */
	public static ArrayList<Eleve> getByAge(int age){
		return EleveDAO.getBy("age", Integer.toString(age));
	}
	
	/**
	 * Retourn un arraylist des élève d'un no chambre donné
	 * @param age
	 * @return ArrayList<Eleve>
	 */
	public static ArrayList<Eleve> getByNo(int no){
		return EleveDAO.getBy("no", Integer.toString(no));
	}
	public static ArrayList<Eleve> getByNo(String no){
		return EleveDAO.getBy("no", no);
	}
	
	/**
	 * Supprimer un élève avec un ID
	 * @param idEleve
	 * @return 
	 * @throws SQLException
	 */
	public static boolean delete(String numEleve) {
		// DELETE FROM eleve WHERE numEleve = eleve.num
		
		try {
			// On prépare notre requete SQL
			String req = "DELETE FROM eleve WHERE eleve.num = '"+numEleve+"' ";
		
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			
			// Execution de la requete et init
			if(DBUtils.getStm().executeUpdate(req) > 0) {
				System.out.println("L'élève num: " + numEleve +" a bien été supprimé");
				return true;
			}else{
				System.out.println("L'élève num: " + numEleve +" n'existe pas ou ne peut pas être supprimé");
				return false;
			}
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	
	/**
	 * Met à jour un élève par son numéro
	 * @param num
	 * @param adresse
	 * @return
	 * @throws SQLException
	 */
	private static int update(String num, String column, String value) throws SQLException {
		int result = -1;
		DBUtils.DBConnexion();
		String req = "UPDATE eleve SET "+column+" = '"+value+"' WHERE num='"+num+"' ";
		result = DBUtils.getStm().executeUpdate(req);
		DBUtils.DBClose();
		return result;
	}
	
	
	
	/**
	 * Met à jour l'adresse d'un élève par son numéro
	 * @param num
	 * @param adresse
	 * @return
	 * @throws SQLException
	 */
	public static int updateAdresse(String num, String adresse){
		try {
			return EleveDAO.update(num, "adresse", adresse);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * Met à jour le nom d'un élève par son numéro
	 * @param num
	 * @param nom
	 * @return
	 * @throws SQLException
	 */
	public static int updateNom(String num, String nom){

		try {
			return EleveDAO.update(num, "nom", nom);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * Met à jour l'age d'un élève par son numéro
	 * @param num
	 * @param age
	 * @return
	 * @throws SQLException
	 */
	public static int updateAge(String num, Integer age){
	
		try {
			return EleveDAO.update(num, "age", age.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * Met à jour (Attribuer une chambre à un élève) le n	°de chambre à un élève
	 * @param no
	 * @param num
	 * @return
	 */
	
	public static boolean updateChambre(String num, int no) {
		String newNo = Integer.toString(no);
		
		if(no == 0) {
			newNo = "NULL";
		}
		try {
			EleveDAO.update(num, "no", newNo );
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	public static boolean updateChambre(String num, String no) {
		return updateChambre(num, Integer.parseInt(no));
	}
	

}
