package com.crea.dev1.jee.ecole.model.beans;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Livre {
	private String cote; // Num identifiant livre
	private String num; // num identifant élève qui à emprunter le livre
	private String titre; // Titre du livre
	private Timestamp datepret; // Date du pret
	private Eleve eleve;
	private SimpleDateFormat dateFormat;
	/**
	 * Constructors
	 * @param cote
	 * @param num
	 * @param titre
	 * @param datepret
	 */
	public Livre(String cote, String num, String titre, Timestamp datepret) {
		this.cote = cote;
		this.num = num;
		this.titre = titre;
		this.datepret = datepret;
		this.eleve = new Eleve("",0,"N/A",0,"");;
	}
	// Init
	public Livre() {
		this("", null,"", null);
	}
	
	/**
	 * Getters and Setters
	 */
	public String getCote() {
		return cote;
	}
	public void setCote(String cote) {
		this.cote = cote;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public Timestamp getDatepret() {
		return datepret;
	}
	public void setDatepret(Timestamp datepret) {
		this.datepret = datepret;
	}
	public Eleve getEleve() {
		return this.eleve;
	}
	
	public void setEleve(Eleve eleve) {
		this.eleve = eleve;
	}
	/**
	 * Set datepret from String
	 * Le format doit être juste sinon une erreur sera affichée
	 * @param date (yyyy-MM-dd HH:mm:ss)
	 */
	public void setDatepret(String date) {
		try {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+2"));
			Date date1=dateFormat.parse(date);
			Timestamp dateTimeStamp=new Timestamp(date1.getTime());			
			this.setDatepret(dateTimeStamp);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	

	/**
	 * Methode pour formater une date dans une requete SQL
	 * @return
	 */
	public String formatDatepretSQL() {
		if(this.datepret == null) {
			return "NULL";
		}
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		String dateString = dateFormat.format(this.datepret);
		return "'"+dateString+"'";
	}
	
	/**
	 * Methode pour formater une date dans un input de formulaire
	 * @return
	 */
	public String formatDatepretInput() {
		if(this.datepret == null) {
			return "";
		}
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		// dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+2"));
		String dateString = dateFormat.format(this.datepret);
		return dateString;
	}
	
	
	
	public String showDatepret() {  
		if(this.datepret==null) {
			return "";
		}else {
			dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			String dateString = dateFormat.format( this.datepret );
			return dateString;
		}
		
	}
	public String showDatepret(String formatDate) {
		if(this.datepret==null) {
			return "";
		}else {
			SimpleDateFormat format = new SimpleDateFormat(formatDate);
			String dateString = format.format( this.datepret );
			return dateString;
		}
		
	}
	
	
	
	
	
}
