package com.crea.dev1.jee.ecole.model.utils;

import java.util.ArrayList;

public interface DAO {

	public abstract void displayAll();
	
	public abstract Object getFromRes();

	public abstract  ArrayList<Object> getAll();

	public abstract ArrayList<Object> getBy(String column, String value);
	
	public abstract void add(Object element);
	
	public abstract void remove(String id);
	
	public abstract void set(String id, String column, String value );
}
