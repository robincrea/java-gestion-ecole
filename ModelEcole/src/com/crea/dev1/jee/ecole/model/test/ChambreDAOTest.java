package com.crea.dev1.jee.ecole.model.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.crea.dev1.jee.ecole.model.beans.Chambre;
import com.crea.dev1.jee.ecole.model.dao.ChambreDAO;

class ChambreDAOTest {
	
	ArrayList<Chambre> chambresRefs = new ArrayList<Chambre>();
	int nextAvailable = ChambreDAO.nextNoAvailable();
	Chambre ref1 = new Chambre(nextAvailable, 150);
	Chambre ref2 = new Chambre((nextAvailable + 1), 78);	
	@BeforeEach
    public void init() {
		assertTrue(ChambreDAO.add(this.ref1));
		assertTrue(ChambreDAO.add(this.ref2));
	
    }
	@AfterEach
    public void destroy() {
		assertTrue(ChambreDAO.delete(ref1.getNo()));
		assertTrue(ChambreDAO.delete(ref2.getNo()));
    }
	


	@Test
	void testUpdatePrix() {
		Float prix = 333.0f; 
		Integer no = ref1.getNo();
		ChambreDAO.updatePrix(no.toString(), prix.toString());		
		Float updatedPrix = ChambreDAO.getByNo(no).getPrix();
		assertEquals(prix, updatedPrix);

	}

	

	@Test
	void testGetAll() {
		assertNotNull( ChambreDAO.getAll());
		assertNotNull( ChambreDAO.getAll().get(0).getPrix());
	}

	@Test
	void testGetByNo() {
		assertNotNull(ChambreDAO.getByNo(ref1.getNo()));
		assertEquals(ref1.getNo(), ChambreDAO.getByNo(ref1.getNo()).getNo());
	}

}
