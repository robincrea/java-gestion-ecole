package com.crea.dev1.jee.ecole.model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import com.crea.dev1.jee.ecole.model.beans.Eleve;
import com.crea.dev1.jee.ecole.model.beans.Livre;
import com.crea.dev1.jee.ecole.model.common.DBUtils;

public class LivreDAO {

	/**
	 * Récupère un arraylist contenant les livre d'une column donnée correspondant à
	 * une valeur donné
	 * 
	 * @param column
	 * @param value
	 * @return
	 */
	private static ArrayList<Livre> getBy(String column, String value) {

		ArrayList<Livre> listeLivre = new ArrayList<Livre>();

		// On prépare notre requete SQL
		String req = "SELECT * FROM livre LEFT JOIN eleve ON livre.num = eleve.num WHERE livre." + column + "='" + value + "'";
		try {
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			DBUtils.setRes(DBUtils.getStm().executeQuery(req));

			while (DBUtils.getRes().next()) {
				Livre livreTemp = LivreDAO.getFromRes(DBUtils.getRes());
				System.out.println("Eleve:"+livreTemp.getNum());
				listeLivre.add(livreTemp);
			}

			// Stopper la connection
			DBUtils.DBClose();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listeLivre;
	}
	
	/**
	 * retourne tous les livres dans un arraylist
	 * @return ArrayList<Eleve> : Liste des livres
	 * @throws SQLException
	 */
	public static ArrayList<Livre> getAll(){
		// SELECT * FROM eleve
		// Connection con  = DBAction.getCon();

	    ArrayList<Livre> listLivres = new ArrayList<Livre>();

		// On prépare notre requete SQL
		String req = "SELECT * FROM livre LEFT JOIN eleve ON livre.num = eleve.num";
		
		// DEMMARER LA CONNECTION
		DBUtils.DBConnexion();
		
		// Execution de la requete et init
		try {
			DBUtils.setRes(DBUtils.getStm().executeQuery(req));
			while(DBUtils.getRes().next()) {
				Livre livreTemp = LivreDAO.getFromRes(DBUtils.getRes());
				
				listLivres.add(livreTemp);
			}

			// Stopper la connection
			DBUtils.DBClose();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listLivres;
								
	}
	
	/**
	 * Séléction d'un livre avec sa cote
	 * @param cote
	 * @return Cote Retourne le livre avec la cote donné
	 */
	public static Livre getByCote(String cote){
		return LivreDAO.getBy("cote", cote).get(0);
	}
	

	private static Livre getFromRes(ResultSet res) throws SQLException {
		Livre livreTemp = new Livre();
		livreTemp.setCote(res.getString(1));
		livreTemp.setNum(res.getString(2));
		livreTemp.setTitre(res.getString(3));
		livreTemp.setDatepret(res.getTimestamp(4));
		
		// public Eleve(String num, int no, String nom, int age, String adresse) {

		Eleve eTemp = new Eleve();
		eTemp.setNum(res.getString(5));
		eTemp.setNo(res.getInt(6));
		eTemp.setNom(res.getString(7));
		eTemp.setAge(res.getInt(8));
		eTemp.setAdresse(res.getString(9));
	
		livreTemp.setEleve(eTemp);
		
		return livreTemp;
	}

	public static boolean add(Livre newLivre) {

		// INSERT INTO `livre` (`cote`, `num`, `titre`, `datepret`) VALUES ('ISBN949304', 'AGUE001', 'Le livre de test', '2019-03-20 14:00:00');
		String num;
		if(newLivre.getNum() == null) {
			num = "null";
		}else {
			num = "'"+newLivre.getNum()+"'";
		}
	
		String livreValues = "'"+newLivre.getCote()+"', "+num+", '"+newLivre.getTitre()+"', "+newLivre.formatDatepretSQL()+"";
		
		// On prépare notre requete SQL
		String req = "INSERT INTO `livre` (`cote`, `num`, `titre`, `datepret`) VALUES ("+livreValues+")";
			
		System.out.println(req);
		try {
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			DBUtils.getStm().executeUpdate(req) ;
			System.out.println("Le livre num: " + newLivre.getCote()  +" a bien été ajout");
			return true;
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Le livre num: " + newLivre.getCote()  +" n'a pas pu être ajouté");
			// e.printStackTrace();
			return false;
		}
		
		
	}
	
	public static boolean delete(String cote) {
		// DELETE FROM livre WHERE livre.cote = cote
		
		try {
			// On prépare notre requete SQL
			String req = "DELETE FROM livre WHERE livre.cote = '"+cote+"' ";
		
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			
			// Execution de la requete et init
			if(DBUtils.getStm().executeUpdate(req) > 0) {
				System.out.println("Le livre avec la cote: " + cote +" a bien été supprimé");
				return true;
			}else{
				System.out.println("Le livre avec la cote: " + cote +" n'existe pas ou ne peut pas être supprimé");
				return false;
			}
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	/**
	 * Met à jour un livre par sa cote
	 * @param num
	 * @param adresse
	 * @return
	 * @throws SQLException
	 */
	private static int update(String cote, String column, String value) throws SQLException {		
		int result = -1;
		DBUtils.DBConnexion();
		String req = "UPDATE livre SET "+column+" = '"+value+"' WHERE cote='"+cote+"' ";
		result = DBUtils.getStm().executeUpdate(req);
		System.out.println("Requete executée");
		DBUtils.DBClose();
		return result;
	}
	
	/**
	 * Met à jour un le titre d'un livre par sa cote
	 * @param cote
	 * @param titre
	 * @return
	 */

	public static int updateTitre(String cote, String titre) {
		// TODO Auto-generated method stub
		try {
			return LivreDAO.update(cote, "titre", titre);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	
	/**
	 * Met à jour un le num d'élève d'emprunt d'un livre par sa cote
	 * @param cote
	 * @param num
	 * @return
	 */

	public static int updateNum(String cote, String num) {
		// TODO Auto-generated method stub
		try {
			return LivreDAO.update(cote, "num", num);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	

	/**
	 * Met à jour un la date de pret d'un livre par sa cote
	 * @param cote
	 * @param num
	 * @return
	 * @throws ParseException 
	 */

	public static int updateDatepret(String cote, String date) throws ParseException{
		// TODO Auto-generated method stub

		try {
			return LivreDAO.update(cote, "datepret", date);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}

}
