package com.crea.dev1.jee.ecole.model.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.crea.dev1.jee.ecole.model.beans.Chambre;
import com.crea.dev1.jee.ecole.model.beans.Eleve;
import com.crea.dev1.jee.ecole.model.common.DBUtils;

public class ChambreDAO {
	
	/**
	 * Récupère un arraylist contenant les chambre d'une column donnée correspondant à une valeur donné
	 * @param column
	 * @param value
	 * @return
	 */
	private static ArrayList<Chambre> getBy(String column, String value){
		
		 ArrayList<Chambre> listeChambre = new ArrayList<Chambre>();

		// On prépare notre requete SQL
		String req = "SELECT no, prix FROM chambre WHERE chambre."+column+"='"+value+"'";
		
		try {
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			DBUtils.setRes(DBUtils.getStm().executeQuery(req));
			
			while(DBUtils.getRes().next()) {
				Chambre elevTemp = ChambreDAO.getFromRes(DBUtils.getRes());
				listeChambre.add(elevTemp);
			}
			
			// Stopper la connection
			DBUtils.DBClose();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listeChambre;
	}
	
	public static int nextNoAvailable() {
		String req = "SELECT max(no) + 1 FROM chambre";
		int next = -1;
		try {
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			DBUtils.setRes(DBUtils.getStm().executeQuery(req));
			DBUtils.getRes().next();
			next = DBUtils.getRes().getInt(1);

			// Stopper la connection
			DBUtils.DBClose();
			
			return next;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Impossible de récupérer le prochain no disponible");
			e.printStackTrace();
			return next;
		}
	}
	/**
	 * Ajout d'un chambre
	 * @param Chambre newChambre
	 * @throws SQLException
	 */
	public static boolean add(Chambre newChambre){
		
		if(newChambre.getNo() <= 0) {
			newChambre.setNo(ChambreDAO.nextNoAvailable());
		}
		
		String chambreValue ="'"+ newChambre.getNo() +"','"+ newChambre.getPrix() +"'";
		
		// On prépare notre requete SQL
		String req = "INSERT INTO `chambre` (`no`, `prix`) VALUES ("+chambreValue+")";
		System.out.println(req);
		try {
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			DBUtils.getStm().executeUpdate(req) ;
			System.out.println("La chambre num: " + newChambre.getNo()  +" a bien été ajout");
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("La chambre num: " + newChambre.getNo()  +" n'as pas pu être ajouté");
			// e.printStackTrace();
			return false;
		}
	
	}
	
	/**
	 * Met à jour une chambre par son numéro
	 * @param num
	 * @param adresse
	 * @return
	 * @throws SQLException
	 */
	private static int update(String no, String column, String value) throws SQLException {
		int result = -1;
		DBUtils.DBConnexion();
		String req = "UPDATE chambre SET "+column+" = '"+value+"' WHERE no='"+no+"' ";
		System.out.println("req update: " +req);
		result = DBUtils.getStm().executeUpdate(req);
		DBUtils.DBClose();
		return result;
	}
	
	/**
	 * Met à jour le prix d'une chambre par son no
	 * @param no
	 * @param value
	 * @return
	 */
	public static boolean updatePrix(String no, String value) {
		try {
			ChambreDAO.update(no, "prix", value);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Supprimer une chambre avec un numero de chambre
	 * @param no
	 * @return 
	 * @throws SQLException
	 */
	public static boolean delete(int no) {
		// DELETE FROM eleve WHERE numEleve = eleve.num
		boolean success = false;
		try {
			// On prépare notre requete SQL
			String req = "DELETE FROM chambre WHERE chambre.no = '"+no+"' ";
		
			// DEMMARER LA CONNECTION
			DBUtils.DBConnexion();
			
			// Execution de la requete et init
			if(DBUtils.getStm().executeUpdate(req) > 0) {
				System.out.println("La chambre no: " + no +" a bien été supprimé");
				success = true;
			}else{
				System.out.println("La chambre no " + no +" n'existe pas ou ne peut pas être supprimé");
				success = false;
			}
		}catch (SQLException e) {
			e.printStackTrace();
			success = false;
		}
		
		// Enlever la chambre supprimé des chambre assigné aux élève 
		if(success) {
			ArrayList<Eleve> byNo = EleveDAO.getByNo(no);
			Iterator<Eleve> elevesIterator = byNo.iterator();
			while(elevesIterator.hasNext()) {
				EleveDAO.updateChambre(elevesIterator.next().getNum(), 0);
			}
		}
		
		return success;
		
	}
	
	
	/**
	 * retourne tous les chambre dans un arraylist
	 * @return ArrayList<Chambre> : Liste des chambres
	 * @throws SQLException
	 */
	public static ArrayList<Chambre> getAll(){
		// SELECT * FROM eleve
		// Connection con  = DBAction.getCon();

	    ArrayList<Chambre> listChambres = new ArrayList<Chambre>();

		// On prépare notre requete SQL
		String req = "SELECT no, prix FROM chambre";
	
		// DEMMARER LA CONNECTION
		DBUtils.DBConnexion();
		
		// Execution de la requete et init
		try {
			DBUtils.setRes(DBUtils.getStm().executeQuery(req));
			while(DBUtils.getRes().next()) {
				Chambre chambreTemp = ChambreDAO.getFromRes(DBUtils.getRes());
				listChambres.add(chambreTemp);
			}
			// Stopper la connection
			DBUtils.DBClose();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return listChambres;
								
	}
	/**
	 * Séléction d'un élève avec un ID
	 * @param idEleve
	 * @return Eleve Retourne l'élève avec le num donné
	 */
	public static Chambre getByNo(Integer no){
		return ChambreDAO.getBy("no", no.toString()).get(0);
	}
	public static Chambre getByNo(String no){
		return ChambreDAO.getBy("no", no).get(0);
	}
	
	private static Chambre getFromRes(ResultSet res) throws SQLException {
		Chambre chambreTemp = new Chambre();
		chambreTemp.setNo(res.getInt(1));
		chambreTemp.setPrix(res.getFloat(2));
		return chambreTemp;
	}

}
