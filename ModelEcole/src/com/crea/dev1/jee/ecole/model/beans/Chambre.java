package com.crea.dev1.jee.ecole.model.beans;

public class Chambre {
	private int no; // Numéro identifiant chambre
	private float prix; // Prix location de la chambre
	
	/**
	 * Contructors
	 * @param no
	 * @param prix
	 */
	public Chambre(int no, float prix) {
		this.no = no;
		this.prix = prix;
	}
	// INITALIZ
	public Chambre() {
		this(0,0);
	}
	
	/*
	 * GETTERS AND SETTERS
	 */
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	
	
}
