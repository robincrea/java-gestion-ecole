package com.crea.dev1.jee.ecole.model.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.crea.dev1.jee.ecole.model.beans.Livre;
import com.crea.dev1.jee.ecole.model.dao.LivreDAO;

class LivreDAOTest {
	
	// public Livre(String cote, String num, String titre, Date datepret) {
	Livre ref1 = new Livre("ISBN209401", null, "Le livre de test 1", new Timestamp(System.currentTimeMillis()));
	Livre ref2 = new Livre("ISBN209403", null, "Le livre de test 2", null);
	
	@BeforeEach
    public void testAdd() {
		assertTrue(LivreDAO.add(this.ref1));
		assertTrue(LivreDAO.add(this.ref2));
		assertFalse(LivreDAO.add(this.ref2));

    }
	@AfterEach
    public void testDelete() {
		assertTrue(LivreDAO.delete(ref1.getCote()));
		assertTrue(LivreDAO.delete(ref2.getCote()));
		assertFalse(LivreDAO.delete(ref2.getCote()));
    }
	
	@Test
	void testAddAndDelete() {
		// ICI on a pas besoin de faire de test 
		// vu qu'ils sont executé dans le After et Before each
		// Si il ne passe pas aucun autre test ne passera
		assertTrue(true);
	}
	
	@Test
	void testgetByCote() {
		Livre testGet = LivreDAO.getByCote(ref1.getCote());
		System.out.println("COTE"+ testGet.getCote());
		assertEquals(testGet.getCote(), ref1.getCote());
		assertEquals(testGet.getTitre(), ref1.getTitre());
	}
	
	@Test
	void testGetAll() {	
		ArrayList<Livre> livres = LivreDAO.getAll();
		Livre current;
		Integer validator = 0;
		Iterator<Livre> livresIterator = livres.iterator();
		try {
			while(livresIterator.hasNext()) {
				current = livresIterator.next();
		
				if(current.getTitre().equals(ref1.getTitre())) {
					validator++;
				}
				if(current.getTitre().equals(ref2.getTitre())) {
					validator++;
				}
			}				
	

		}
		catch(Exception e) {
			validator = 0;
		}
		assertTrue((validator==2));
	}

}
