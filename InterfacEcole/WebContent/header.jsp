<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${param.pageTitle}</title>
<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" > 
<link href="${pageContext.request.contextPath}/assets/css/jquery-ui.min.css" rel="stylesheet" > 
<link href="${pageContext.request.contextPath}/assets/css/jquery-ui-timepicker-addon.min.css" rel="stylesheet" > 
<link href="${pageContext.request.contextPath}/assets/css/custom.css" rel="stylesheet" > 

</head>
<body>

	
<nav class="custom-nav">
	<div class="container">
		<ul>
		    <li>
		        <a href="${pageContext.request.contextPath}/eleves">
		            Eleves
		        </a>
		    </li>
		    <li>
		        <a href="${pageContext.request.contextPath}/livres">
		            Livres
		        </a>
		    </li>
		    <li>
		        <a href="${pageContext.request.contextPath}/chambres">
		            Chambres
		        </a>
		    </li>
		</ul>
	</div>
</nav>
