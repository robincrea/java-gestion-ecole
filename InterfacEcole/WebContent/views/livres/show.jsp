<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="livre" class="com.crea.dev1.jee.ecole.model.beans.Livre" scope="request" />  
<jsp:setProperty name="livre" property="*" /> 

 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Afficher ${param.livre.getTitre()}"/>
 </jsp:include>

<div class="container">
	<div class="row justify-content-md-center mt-5">
		<div class="col-md-8">
			Titre: <jsp:getProperty property="titre" name="livre" /><br/>
			Cote: <jsp:getProperty property="cote" name="livre" /><br/>
			Emprunté par: <%= livre.getEleve().getNom() %><br/>
			Emprunté le: <%= livre.showDatepret() %><br/>
		</div>
	</div>
</div>
 <jsp:include page="../../footer.jsp" />