<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.ecole.model.beans.Livre"%> 
<%ArrayList<Livre> livres = (ArrayList<Livre>) request.getAttribute("livres");%>
 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Liste des Livres"/>
 </jsp:include>
 <jsp:include page="../../alert.jsp" />
 

<div class="container mt-5">
	<div class="d-flex justify-content-between align-items-center">
		<h1>Liste des Livres</h1>
		<a class="btn btn-primary" href="${pageContext.request.contextPath}/livres/add">Ajouter</a>
	</div>
		<table class="table table-striped">
	  		<thead>
	  			<tr>
	  				<th>Cote</th>
	  				<th>Titre</th>
	  				<th>Emprunté par</th>
	  				<th>date</th>
	  				<th class="text-right">Actions</th>
	  			</tr>
	  		</thead>
	  		<tbody>
			<%for(Livre l:livres){%> 
	            <tr> 
	            	<td><%= l.getCote() %></td>
	                <td>
	                	<a href="${pageContext.request.contextPath}/livres/show/<%= l.getCote() %>">
	                		<%= l.getTitre() %>
	                	</a>
	                </td>
	                <td><%= l.getEleve().getNom() %></td>
	                
	                <td>
	                	
	                		<%= l.showDatepret() %>
	                	
	                </td> 
	       
	                <td class="text-right">
	                	<a class="btn btn-success" href="${pageContext.request.contextPath}/livres/show/<%= l.getCote() %>">
	                		Afficher
	                	</a>
	                	
	                	<a class="btn btn-primary" href="${pageContext.request.contextPath}/livres/edit/<%= l.getCote() %>">
	                		Modifier
	                	</a>
	                	
	                	<a class="btn btn-danger" href="${pageContext.request.contextPath}/LivreController?action=delete&cote=<%= l.getCote() %>">
	                		Supprimer
	                	</a>
	                </td>
	            </tr> 
	            <%}%> 
	        </tbody>
	      </table>
      </div>
 <jsp:include page="../../footer.jsp" />