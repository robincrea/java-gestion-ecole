<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.crea.dev1.jee.ecole.model.beans.Eleve"%> 

<jsp:useBean id="eleve" class="com.crea.dev1.jee.ecole.model.beans.Eleve" scope="request" />  
<jsp:setProperty name="eleve" property="*" />   
 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Ajouter un &eacute;l&egrave;ve"/>
 </jsp:include>
 <%@page import="java.util.ArrayList"%>
 <% ArrayList<Eleve> eleves = (ArrayList<Eleve>) request.getAttribute("eleves"); %>

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-md-6">
			<form method="POST" action="${pageContext.request.contextPath}/LivreController?action=create">

			 
			 <div class="form-group">
			   <label for="cote">Cote</label>
			   <input type="text" name="cote" class="form-control" id="cote">
			 </div>
			 
			 <div class="form-group">
			   <label for="titre">Titre du livre</label>
			   <input type="text" name="titre" class="form-control" id="titre" required>
			 </div>
			 
			 <div class="form-group">
			   <label for="num">Numéro de l'élève (qui à emprunté le livre)</label>
			   <select name="num" class="form-control" id="num">
			   		<option value="">Séléctionner un élève</option>
			   
			     <%for(Eleve e:eleves){%> 
		   			<option value="<%= e.getNum() %>"><%= e.getNom() %></option>
		   		 <%}%>
			   </select>
			  
			 </div>
			 
			
			 
			 <div class="form-group">
			   <label for="datepret">Date de prêt</label>
			   <input type="text" name="datepret" class="form-control datepicker" id="datepret">
			 </div>
			 
	
			 
			 <input class="btn btn-primary" type="submit">
			 
			 
			</form>
		</div>
	</div>
</div>

<jsp:include page="../../footer.jsp" />

