<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="livre" class="com.crea.dev1.jee.ecole.model.beans.Livre" scope="request" />  
<jsp:setProperty name="livre" property="*" />
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.ecole.model.beans.Eleve"%> 
<% ArrayList<Eleve> eleves = (ArrayList<Eleve>) request.getAttribute("eleves"); %>

 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Modifier un &eacute;l&egrave;ve"/>
 </jsp:include>

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-md-6">
			<form method="POST" action="${pageContext.request.contextPath}/LivreController?action=update">

			 
			 <div class="form-group">
			   <label for="coteRef">Cote du livre</label>
			   <input type="text" name="coteRef" value="<%= livre.getCote() %>" class="form-control" id="coteRef" disabled>
			   <input type="text" name="cote" value="<%= livre.getCote() %>" class="form-control" id="cote" hidden>
			 </div>
			 <div class="form-group">
			   <label for="titre">Titre du livre</label>
			   <input type="text" name="titre" value="<%= livre.getTitre() %>" class="form-control" id="titre" required>
			 </div>
			 
			 <div class="form-group">
			   <label for="num">Numéro de l'élève (qui à emprunté le livre)</label>
			   <select name="num" class="form-control" id="num">
			   		<option value="">Séléctionner un élève</option>
			   
			     <%for(Eleve e:eleves){%> 
			     	<% if( e.getNum().equals(livre.getNum()) ){ %>
			     		<option value="<%= e.getNum() %>" selected><%= e.getNom() %></option>
			     	<%}else{ %>
		   			<option value="<%= e.getNum() %>"><%= e.getNom() %></option>
		   			<%} %>
		   		 <%}%>
			   </select>
			  
			 </div>
			 <div class="form-group">
			   <label for="datepret">Date de prêt</label>
			   <input type="text" name="datepret" value="<%= livre.formatDatepretInput() %>" class="form-control datepicker" id="datepret">
			 </div>
			 
			 <input class="btn btn-primary" type="submit">
			 
			 
			</form>
		</div>
	</div>
</div>

<jsp:include page="../../footer.jsp" />

