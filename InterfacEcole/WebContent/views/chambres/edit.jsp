<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="chambre" class="com.crea.dev1.jee.ecole.model.beans.Chambre" scope="request" />  
<jsp:setProperty name="chambre" property="*" />
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.ecole.model.beans.Eleve"%> 

 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Modifier une chambre"/>
 </jsp:include>

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-md-6">
			<form method="POST" action="${pageContext.request.contextPath}/ChambreController?action=update">
			 <div class="form-group">
			   <label for="noref">Numero de chambre</label>
			   <input type="text" name="noref" class="form-control" id="noref" value="<%= chambre.getNo() %>" disabled>
			   <input type="text" name="no" class="form-control" id="no" value="<%= chambre.getNo() %>" hidden> 
			 </div>
			 <div class="form-group">
			   <label for="prix">Prix de la chambre (EN CHF)</label>
			   <input type="text" name="prix" class="form-control" value="<%= chambre.getPrix() %>" id="prix" pattern="^[0-9\.]*$" title="p.ex:122.5" required>
			 </div>
			 <input class="btn btn-primary" type="submit">
			</form>
		</div>
	</div>
</div>

<jsp:include page="../../footer.jsp" />

