<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    
 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Ajouter une chambre"/>
 </jsp:include>
 

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-md-6">
			<form method="POST" action="${pageContext.request.contextPath}/ChambreController?action=create">

			 
			 <div class="form-group">
			   <label for="no">Numero de chambre</label>
			   <input type="text" name="no" class="form-control" id="no" value="<%= request.getAttribute("next") %>">
			 </div>
			 
			 <div class="form-group">
			   <label for="prix">Prix de la chambre (EN CHF)</label>
			   <input type="text" name="prix" class="form-control" id="prix" pattern="^[0-9\.]*$" title="p.ex:122.5" required>
			 </div>
			 
			
			 
			 <input class="btn btn-primary" type="submit">
			 
			 
			</form>
		</div>
	</div>
</div>

<jsp:include page="../../footer.jsp" />

