<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="chambre" class="com.crea.dev1.jee.ecole.model.beans.Chambre" scope="request" />  

<jsp:setProperty name="livre" property="*" />
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.ecole.model.beans.Eleve"%> 
<% ArrayList<Eleve> eleves = (ArrayList<Eleve>) request.getAttribute("eleves"); %>

 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Afficher ${param.livre.getTitre()}"/>
 </jsp:include>

<div class="container">
	<div class="row justify-content-md-center mt-5">
		<div class="col-md-8">
			Numéro de chambre: <jsp:getProperty property="no" name="chambre" /><br/>
			Prix de la chambre: <jsp:getProperty property="prix" name="chambre" /> CHF<br/>
			Eleve dans la chambres:
			<ul>
			
			<%for(Eleve e:eleves){%> 
				<li><%= e.getNom() %></li>
			<% } %>
			</ul>
			
		</div>
	</div>
</div>
 <jsp:include page="../../footer.jsp" />