<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.ecole.model.beans.Chambre"%> 
<%
	ArrayList<Chambre> chambres = (ArrayList<Chambre>) request.getAttribute("chambres");

%>


 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Liste des Livres"/>
 </jsp:include>
 
	
<jsp:include page="../../alert.jsp" />

 

<div class="container mt-5">
	<div class="d-flex justify-content-between align-items-center">
		<h1>Liste des chambres</h1>
		<a class="btn btn-primary" href="${pageContext.request.contextPath}/chambres/add">Ajouter</a>
	</div>
		<table class="table table-striped">
	  		<thead>
	  			<tr>
	  				<th>No</th>
	  				<th>Prix</th>
	  				<th class="text-right">Actions</th>
	  			</tr>
	  		</thead>
	  		<tbody>
			<%for(Chambre chambre:chambres){%> 
	            <tr> 
	                <td>
	                	<a href="${pageContext.request.contextPath}/chambres/show/<%= chambre.getNo() %>">
	                		<%= chambre.getNo() %>
	                	</a>
	                </td>
		            	<td><%= chambre.getPrix() %></td>
	
	                <td class="text-right">
	                	<a class="btn btn-success" href="${pageContext.request.contextPath}/chambres/show/<%= chambre.getNo() %>">
	                		Afficher
	                	</a>
	                	<a class="btn btn-primary" href="${pageContext.request.contextPath}/chambres/edit/<%= chambre.getNo() %>">
	                		Modifier
	                	</a>
	                	
	                	<a class="btn btn-danger" href="${pageContext.request.contextPath}/ChambreController?action=delete&no=<%= chambre.getNo() %>">
	                		Supprimer
	                	</a>
	                </td>
	            </tr> 
	            <%}%> 
	        </tbody>
	      </table>
      </div>
 <jsp:include page="../../footer.jsp" />