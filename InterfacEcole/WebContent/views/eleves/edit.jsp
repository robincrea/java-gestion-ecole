<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="eleve" class="com.crea.dev1.jee.ecole.model.beans.Eleve" scope="request" />  
<jsp:setProperty name="eleve" property="*" />  

<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.ecole.model.beans.Chambre"%>  
<% ArrayList<Chambre> chambres = (ArrayList<Chambre>) request.getAttribute("chambres"); %>
 
 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Modifier un &eacute;l&egrave;ve"/>
 </jsp:include>

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-md-6">
			<form method="POST" action="${pageContext.request.contextPath}/EleveController?action=update">

			 
			 <div class="form-group">
			   <label for="num">Numéro d'élève</label>
			   <input type="text" name="numRef" value="<%= eleve.getNum() %>" class="form-control" id="num" disabled>
			   <input type="text" name="num" value="<%= eleve.getNum() %>" class="form-control" id="num" hidden>
			 </div>
			 
			
   			
   			 <div class="form-group">
			   <label for="no">Numéro de chambre</label>
			   <select name="no" class="form-control" id="no">
			   		<option value="0">Aucune chambre</option>
				     <%for(Chambre c:chambres){%> 
				      	<% if( c.getNo() == eleve.getNo() ){ %>
				     		<option value="<%= c.getNo() %>" selected><%= c.getNo() %></option>
				     	<%}else{ %>
			   				<option value="<%= c.getNo() %>"><%= c.getNo() %></option>
			   			<%} %>
			   		<% } %>
			   </select>
			 </div>
			 
			 <div class="form-group">
			   <label for="adresse">Nom de l'élève</label>
			   <input type="text" name="nom" value="<%= eleve.getNom() %>" class="form-control" id="nom" required>
			 </div>
			 
			 <div class="form-group">
			   <label for="age">Age de l'élève</label>
			   <input type="number" name="age" value="<%= eleve.getAge() %>" class="form-control" id="age" required>
			 </div>
			 
			 
			 <div class="form-group">
			   <label for="adresse">adresse d'élève</label>
			   <input type="text" name="adresse" value="<%= eleve.getAdresse() %>" class="form-control" id="adresse" required>
			 </div>
			 
			 <input class="btn btn-primary" type="submit">
			 
			 
			</form>
		</div>
	</div>
</div>

<jsp:include page="../../footer.jsp" />

