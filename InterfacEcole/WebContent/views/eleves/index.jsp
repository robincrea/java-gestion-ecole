<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.ecole.model.beans.Eleve"%> 
<% ArrayList<Eleve> eleves = (ArrayList<Eleve>) request.getAttribute("eleves"); %>

 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Liste des &eacute;l&egrave;ves"/>
 </jsp:include>
 
<jsp:include page="../../alert.jsp" />

	<div class="container mt-5">
	<div class="d-flex justify-content-between align-items-center">
		<h1>Liste des élèves</h1>
		<a class="btn btn-primary" href="${pageContext.request.contextPath}/eleves/add">Ajouter</a>
	</div>
		<table class="table table-striped">
	  		<thead>
	  			<tr>
	  				<th>Num</th>
	  				<th>Nom</th>
	  				<th>N° Chambre</th>
	  				<th>Age</th>
	  				<th>Adresse</th>
	  				<th class="text-right">Actions</th>
	  			</tr>
	  		</thead>
	  		<tbody>
	        <%
			for(Eleve e:eleves){%> 
	        <%-- Arranging data in tabular form 
	        --%> 
	            <tr> 
	            	<td><%= e.getNum() %></td>
	                <td>
	                	<a href="${pageContext.request.contextPath}/eleves/show/<%= e.getNum() %>">
	                		<%= e.getNom() %>
	                	</a>
	                </td> 
	                <td><%= e.getNo() %></td>
	                <td><%= e.getAge() %></td> 
	                <td><%= e.getAdresse() %></td> 
	                <td class="text-right">
	                	<a class="btn btn-success" href="${pageContext.request.contextPath}/eleves/show/<%= e.getNum() %>">
	                		Afficher
	                	</a>
	                	<a class="btn btn-primary" href="${pageContext.request.contextPath}/eleves/edit/<%= e.getNum() %>">
	                		Modifier
	                	</a>
	                	
	                	<a class="btn btn-danger" href="${pageContext.request.contextPath}/EleveController?action=delete&num=<%= e.getNum() %>">
	                		Supprimer
	                	</a>
	                </td>
	            </tr> 
	            <%}%> 
	        </tbody>
	      </table>
      </div>


 <jsp:include page="../../footer.jsp" />