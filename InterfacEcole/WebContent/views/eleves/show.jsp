<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="eleve" class="com.crea.dev1.jee.ecole.model.beans.Eleve" scope="request" />  
<jsp:setProperty name="eleve" property="*" />   
 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Afficher ${param.eleve.getNom()}"/>
 </jsp:include>

<div class="container">
	<div class="row justify-content-md-center mt-5">
		<div class="col-md-8">
			Name: <jsp:getProperty property="nom" name="eleve" /><br/>
			Age: <jsp:getProperty property="age" name="eleve" /><br/>
			Adresse: <jsp:getProperty property="adresse" name="eleve" /><br/>
		</div>
	</div>
</div>
 <jsp:include page="../../footer.jsp" />