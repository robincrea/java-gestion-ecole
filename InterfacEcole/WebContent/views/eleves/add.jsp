<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="eleve" class="com.crea.dev1.jee.ecole.model.beans.Eleve" scope="request" />  
<jsp:setProperty name="eleve" property="*" /> 
<%@page import="java.util.ArrayList"%>
<%@page import="com.crea.dev1.jee.ecole.model.beans.Chambre"%>  
<% ArrayList<Chambre> chambres = (ArrayList<Chambre>) request.getAttribute("chambres"); %>
 
 <jsp:include page="../../header.jsp">
     <jsp:param name="pageTitle" value="Ajouter un &eacute;l&egrave;ve"/>
 </jsp:include>

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-md-6">
			<form method="POST" action="${pageContext.request.contextPath}/EleveController?action=create">

			 
			 <div class="form-group">
			   <label for="num">Numéro d'élève</label>
			   <input type="text" name="num" class="form-control" id="num" disabled>
			 </div>
			 
			 
			 <div class="form-group">
			   <label for="no">Numéro de chambre</label>
			   <select name="no" class="form-control" id="no">
			   		<option value="0">Aucune chambre</option>
				     <%for(Chambre c:chambres){%> 
			   			<option value="<%= c.getNo() %>"><%= c.getNo() %></option>
			   		<% } %>
			   </select>
			 </div>
			 
			 <div class="form-group">
			   <label for="adresse">Nom de l'élève</label>
			   <input type="text" name="nom" class="form-control" id="nom" required>
			 </div>
			 
			 <div class="form-group">
			   <label for="age">Age de l'élève</label>
			   <input type="number" name="age" class="form-control" id="age" required>
			 </div>
			 
			 
			 <div class="form-group">
			   <label for="adresse">adresse d'élève</label>
			   <input type="text" name="adresse" class="form-control" id="adresse" required>
			 </div>
			 
			 <input class="btn btn-primary" type="submit">
			 
			 
			</form>
		</div>
	</div>
</div>

<jsp:include page="../../footer.jsp" />

