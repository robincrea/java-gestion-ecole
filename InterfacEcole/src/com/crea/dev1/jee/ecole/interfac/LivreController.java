package com.crea.dev1.jee.ecole.interfac;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.TimeZone;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crea.dev1.jee.ecole.model.beans.Livre;
import com.crea.dev1.jee.ecole.model.dao.EleveDAO;
import com.crea.dev1.jee.ecole.model.dao.LivreDAO;
import com.crea.dev1.jee.ecole.utils.RequestUtils;

/**
 * Servlet implementation class LivreController
 */
@WebServlet("/LivreController")
public class LivreController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    //Get default time zone
    TimeZone timeZone = TimeZone.getDefault();

    //Setting default time zone
    /*
    System.out.println("2. Setting UTC as default time zone");
    TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
    //Setting UTC time zone
    TimeZone.setDefault(utcTimeZone);

    //Now get default time zone
    timeZone = TimeZone.getDefault();
    */

    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LivreController() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			processRequest(request, response);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
	
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
			
			request.setCharacterEncoding("UTF-8");

			RequestUtils.defineTimeZone();

	
    		ArrayList<Livre> livres;
    		String forward = "index.jsp";
    		
    		String[] actionRequest = RequestUtils.getActionRequest(request);
    		String action = actionRequest[0];
    		String actionValue = actionRequest[1];
    		
    		
    		String num;
    		String cote;
    		String dateString;
    	
    		
    		switch(action) {
    			case("index"):
    				livres = LivreDAO.getAll();
    			
	    			request.setAttribute("livres", livres);
					forward = "index.jsp";
					break;
    			case("add"):
					forward = "add.jsp";
    				request.setAttribute("eleves", EleveDAO.getAll());
    				break;
    			case("edit"):
    				forward = "edit.jsp";
    				request.setAttribute("eleves", EleveDAO.getAll());
    				request.setAttribute("livre", LivreDAO.getByCote(actionValue));

    				break;
    			case("create"):
    				Livre newLivre = new Livre();
    				num = request.getParameter("num");
    				
    				if(! request.getParameter("datepret").isEmpty()) {
    					// Récupération de la date depuis le formulaire et ajout des secondes
    					dateString = request.getParameter("datepret") + ":00";
						newLivre.setDatepret(dateString);	 
    				}
	    			if(! num.isEmpty()) {
	    				newLivre.setNum(num);
					}
	    			newLivre.setTitre(request.getParameter("titre"));
	    			newLivre.setCote(request.getParameter("cote"));
	    			
	    			LivreDAO.add(newLivre);
	    			
	    			request.setAttribute("livres", LivreDAO.getAll());
					forward = "index.jsp";

	    			response.setStatus(HttpServletResponse.SC_FOUND);//302
	    		    response.setHeader("Location", request.getContextPath()+"/livres/");
	    			
    				break;
    			case("update"):
    				cote = request.getParameter("cote");

    				LivreDAO.updateTitre(cote, request.getParameter("titre"));

    				LivreDAO.updateNum(cote, request.getParameter("num"));
    				
					dateString = request.getParameter("datepret") + ":00";
					try {
						LivreDAO.updateDatepret(cote, dateString);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    			
        		    request.setAttribute("livres", LivreDAO.getAll());
					forward = "index.jsp";
					
    				response.setStatus(HttpServletResponse.SC_FOUND);//302
        		    response.setHeader("Location", request.getContextPath()+"/livres/");
        		    

    				break;
				case("delete"):
					LivreDAO.delete(request.getParameter("cote"));
					request.setAttribute("livres", LivreDAO.getAll());
	    			response.setStatus(HttpServletResponse.SC_FOUND);//302
	    		    response.setHeader("Location", request.getContextPath()+"/livres/");
					break;
				case("show"):    			
	    			Livre ltemp = LivreDAO.getByCote(actionValue);
					request.setAttribute("livre", ltemp);
					// request.setAttribute("eleve", ltemp.getEleve());
					
	    			forward = "show.jsp";
	    			break;
    			default:
    				livres = LivreDAO.getAll();
	    			request.setAttribute("livres", livres);
					forward = "index.jsp";
    		}
    		
    		
    		
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/views/livres/"+forward);
    		dispatcher.forward(request, response);
    }
	
	
}
