package com.crea.dev1.jee.ecole.interfac;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crea.dev1.jee.ecole.model.beans.Chambre;
import com.crea.dev1.jee.ecole.model.beans.Eleve;
import com.crea.dev1.jee.ecole.model.dao.ChambreDAO;
import com.crea.dev1.jee.ecole.model.dao.EleveDAO;
import com.crea.dev1.jee.ecole.utils.Alert;
import com.crea.dev1.jee.ecole.utils.RequestUtils;

import java.util.UUID;


/**
 * Servlet implementation class EleveController
 */
@WebServlet("/EleveController")
public class EleveController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EleveController() {
        super();
    }

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
    	request.setCharacterEncoding("UTF-8");

    	
    	/* Récupérer les paramètre de l'url path */
    	
    	String[] actionRequest = RequestUtils.getActionRequest(request);
		String action = actionRequest[0];
		String actionValue = actionRequest[1];

		
    	/** Procéder au traitement en fonction de l'action */
		String forward = "accueil.jsp";
		
		/*Déclaration des variable*/
		String num;
		Integer no;
		String noString;
		String nom;
		Integer age;
		String adresse;
		ArrayList<Eleve> eleves;
		ArrayList <Chambre> chambres;
		boolean success;
		Alert alert = new Alert();

    	switch(action) {
    		case("auth"):
    			// TODO
    			break;
    		case("test"):
    			// TODO
				forward = "test.jsp";
    			break;

    		case("index"):
    			// TODO	
    			eleves = EleveDAO.getAll();
    			request.setAttribute("eleves", eleves);
				forward = "index.jsp";
    			break;
    		case("add"):
    			// TODO

    			eleves = EleveDAO.getAll();
    			request.setAttribute("eleves", eleves);
				chambres = ChambreDAO.getAll();
				request.setAttribute("chambres", chambres);

				forward = "add.jsp";
				break;
    		case("edit"):
    			num = actionValue;
    			
				if(num.contentEquals("") || num.equals(null)) {
					// TODO Log error recupération
				}else {
					
					Eleve etemp = EleveDAO.getByNum(num);
					chambres = ChambreDAO.getAll();
					if(etemp.equals(null)) {
						// Forward un résultat: Eleve inexistant
						forward = "error.jsp";
	
					}else {
						// Forward un résultat: Eleve
						// Intégrer le bean "eleve"	
						request.setAttribute("eleve", etemp);
						request.setAttribute("chambres", chambres);

						forward = "edit.jsp";
					}
				}
    			break;
    		case("show"):    			
    			Eleve etemp = EleveDAO.getByNum(actionValue);
    			request.setAttribute("eleve", etemp);
    			forward = "show.jsp";
    			break;
    		case("create"):
    			UUID uuid = UUID.randomUUID();
				num = uuid.toString();
				nom = request.getParameter("nom");
				age = Integer.parseInt(request.getParameter("age"));
				adresse = request.getParameter("adresse");
    			noString = request.getParameter("no");
    			Eleve newEleve = new Eleve(num,noString,nom,age,adresse);
    			
    			success = EleveDAO.add(newEleve);
    			if(success) {
    				alert = new Alert("addSuccess", nom);
    			}else {
    				alert = new Alert("addError", nom);

    			}
    			request.setAttribute("eleves", EleveDAO.getAll());
				forward = "index.jsp";
				
    			break;
    		case("update"):
    			
    			num = request.getParameter("num");
    			nom = request.getParameter("nom");
    			noString = request.getParameter("no");
				age = Integer.parseInt(request.getParameter("age"));
    			adresse = request.getParameter("adresse");
    			
    			EleveDAO.updateAdresse(num, adresse);
    			EleveDAO.updateNom(num, nom);
    			EleveDAO.updateAge(num, age);
    			EleveDAO.updateChambre(num, noString);
				
    			alert = new Alert("updateSuccess", nom);

    			
    			request.setAttribute("eleves", EleveDAO.getAll());
    			forward = "index.jsp";
    			
    			break;
    		case("delete"):
    			// TODO
    			success = EleveDAO.delete(request.getParameter("num"));
	    		if(success) {
	    		    alert = new Alert("deleteSuccess", request.getParameter("num"));
				}else {
	    		    alert = new Alert("deleteError", request.getParameter("num"));
				}
	    		request.setAttribute("eleves", EleveDAO.getAll());
				forward = "index.jsp";
				
    			break;
    		
    		case(""):
    			// TODO	

    			eleves = EleveDAO.getAll();
    			request.setAttribute("eleves", eleves);
				forward = "index.jsp";

    			break;
    		default:
    			// TODO
    			eleves = EleveDAO.getAll();
    			request.setAttribute("eleves", eleves);
				forward = "index.jsp";
    			break;
    	}
    	
		request.setAttribute("alert", alert);

    	RequestDispatcher dispatcher = request.getRequestDispatcher("/views/eleves/"+forward);
		dispatcher.forward(request, response);
		

    	
    	
    }
    
    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			processRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
	
	

}