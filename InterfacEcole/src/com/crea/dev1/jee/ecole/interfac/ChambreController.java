package com.crea.dev1.jee.ecole.interfac;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crea.dev1.jee.ecole.model.beans.Chambre;
import com.crea.dev1.jee.ecole.model.dao.ChambreDAO;
import com.crea.dev1.jee.ecole.model.dao.EleveDAO;
import com.crea.dev1.jee.ecole.utils.Alert;
import com.crea.dev1.jee.ecole.utils.RequestUtils;

/**
 * Servlet implementation class ChambreController
 */
@WebServlet("/ChambreController")
public class ChambreController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChambreController() {
        super();
        // TODO Auto-generated constructor stub
    }
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			processRequest(request, response);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
	
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
			
			request.setCharacterEncoding("UTF-8");
			RequestUtils.defineTimeZone();
		
			ArrayList<Chambre> chambres;
			String forward = "index.jsp";
			
			String[] actionRequest = RequestUtils.getActionRequest(request);
			String action = actionRequest[0];
			String actionValue = actionRequest[1];
			String no;
			boolean success;
			Alert alert = new Alert();
			
			switch(action) {
			case("index"):
				chambres = ChambreDAO.getAll();
    			request.setAttribute("chambres", chambres);
				forward = "index.jsp";
				break;
			case("add"):
				forward = "add.jsp";
				int nextAvailable = ChambreDAO.nextNoAvailable();
				request.setAttribute("next", nextAvailable);
				break;
			case("edit"):
				forward = "edit.jsp";
				request.setAttribute("chambre", ChambreDAO.getByNo(actionValue));
				break;
			case("show"):
				forward = "show.jsp";
				request.setAttribute("eleves", EleveDAO.getByNo(actionValue));
				request.setAttribute("chambre", ChambreDAO.getByNo(actionValue));
				break;
			case("update"):
				success = ChambreDAO.updatePrix(request.getParameter("no"), request.getParameter("prix"));
				if(success) {
	    		    alert = new Alert("updateSuccess", request.getParameter("no"));
				}else {
	    		    alert = new Alert("updateError", request.getParameter("no"));
				}
				forward = "index.jsp";
				chambres = ChambreDAO.getAll();
    			request.setAttribute("chambres", chambres);
    		    break;
			case("create"):
				
				if(request.getParameterMap().containsKey("no")) {
					Chambre cTemp = new Chambre();
					cTemp.setNo(Integer.parseInt(request.getParameter("no")));
					cTemp.setPrix(Float.parseFloat(request.getParameter("prix")));
					success = ChambreDAO.add(cTemp);
					if(success) {
						alert = new Alert("addSuccess", request.getParameter("no"));
					}else {
						alert = new Alert("exist", request.getParameter("no"));
					}
					
				}else {
					response.setStatus(HttpServletResponse.SC_FOUND);//302
	    		    response.setHeader("Location", request.getContextPath()+"/chambres");
				}
				
				
				
				forward = "index.jsp";
				chambres = ChambreDAO.getAll();
    			request.setAttribute("chambres", chambres);
    		    break;
			case("delete"):
				no = request.getParameter("no");
				success = ChambreDAO.delete(Integer.parseInt(no));
				if(success) {
	    		    alert = new Alert("deleteSuccess", request.getParameter("no"));
				}else {
	    		    alert = new Alert("deleteError", request.getParameter("no"));
				}
				forward = "index.jsp";
				chambres = ChambreDAO.getAll();
    			request.setAttribute("chambres", chambres);
				break;
				
			}
			

			request.setAttribute("alert", alert);
			
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/views/chambres/"+forward);
    		dispatcher.forward(request, response);
	}
}
