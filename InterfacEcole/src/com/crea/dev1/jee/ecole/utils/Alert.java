package com.crea.dev1.jee.ecole.utils;


public class Alert {
	private String type;
	private String message;
	private boolean isDisplay;
	
	/**
	 * CONSTRUCTOR
	 */
	
	public Alert(String type, String value) {
		
		this.isDisplay = true;
		if(type==null) {
			type = "";
		}
		switch(type) {
			case("exist"):
				this.type = "danger";
				this.message = "Impossible d'ajouter l'élément suivant: <strong>"+value+"</strong> celui-ci existe probablement déjà";
				break;
			case("addError"):
				this.type = "danger";
				this.message = "Impossible d'ajouter l'élément suivant: <strong>"+value+"</strong>";
				break;
			case("addSuccess"):
				this.type = "success";
				this.message = "L'élément suivant à été ajouté avec succès: " + value;
				break;
			case("updateSuccess"):
				this.type = "success";
				this.message = "L'élément suivant à été mis à jour avec succès: " + value;
				break;
			case("updateError"):
				this.type = "danger";
				this.message = "L'élément suivant n'a pas pu etre mis à jour: " + value;
				break;
			case("deleteSuccess"):
				this.type = "danger";
				this.message = "L'élément suivant à été supprimé: " + value;
				break;
			case("deleteError"):
				this.type = "danger";
				this.message = "Suppression impossible, l'élément ne doit probablement pas exister";
				break;
			default:
				this.type = null;
				this.isDisplay = false;
				this.message = null;
		}
		
	}
	public Alert(String type) { 
		this(type, null);
	}
	public Alert() { 
		this(null, null);
	}
	
	
	
	/**
	 * Getter and setters
	 */
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isDisplay() {
		return isDisplay;
	}
	public void setDisplay(boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	
	
	
	
	
}
